class ig.VoronoiOverlay
  (@graph) ->

  prepare: ->
    @svg = @graph.parentElement.append \svg
      ..attr \class \voronoi
      ..attr \width @graph.fullWidth
      ..attr \height @graph.fullHeight
    @drawing = @svg
    @prepared = yes
    @voronoi = d3.geom.voronoi!
      ..x ~> @graph.margin.left + @graph.xScale it.x
      ..y ~> @graph.margin.top + @graph.yScale it.y
      ..clipExtent [[0, 0], [@graph.fullWidth, @graph.fullHeight]]

  draw: (points) ->
    @prepare! if not @prepared
    voronoiPolygons = @voronoi points
      .filter -> it
    @drawing.selectAll \path .remove!
    @drawing.selectAll \path .data voronoiPolygons .enter!append \path
      ..attr \d polygon

polygon = ->
  "M#{it.join "L"}Z"

