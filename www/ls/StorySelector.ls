class ig.StorySelector
  (@baseElement, @graph, @stories, @data) ->
    @container = @baseElement.append \div
      ..attr \class \story-selector
    @items = @container.append \div
      ..attr \class \items
      ..style \width "calc(100% * #{@stories.length})"

    @items.selectAll \div.item .data @stories .enter!append \div
      ..attr \class \item
      ..style \width "calc(100% / #{@stories.length})"
      ..append \h2
        ..html (.heading)
      ..append \p
        ..html (.text)
    @nextButton = @container.append \a
      ..attr \href \#
      ..html "›"
      ..on \click ~>
        d3.event.preventDefault!
        @displayId @currentIndex + 1
    @prevButton = @container.append \a
      ..attr \class \left
      ..attr \href \#
      ..html "‹"
      ..on \click ~>
        d3.event.preventDefault!
        @displayId @currentIndex - 1
    @points = @container.append \ul .attr \class \points
        .selectAll \li .data @stories .enter!append \li
          ..on \click (d, i) ~> @displayId i
    id = if window.location.hash
      parseInt do
        window.location.hash.substr 1
        10
    else
      0
    id = id || 0
    @displayId id

  displayId: (index) ->
    index %%= @stories.length
    @currentIndex = index
    story = @stories[index]
    @items.style \left "#{-100 * index}%"
    @graph.setDisplay story.pricesOrCounts
    id = if story.groups
      len = story.groups.0.length
      parentLen = if len == 3 or len == 7 then len - 2 else len - 1
      story.groups.0.substr 0, parentLen
    else
      ""
    @graph.draw @data[id]
    if story.years
      @graph.highlightYear story.years.0, story.years.1
    if story.groups
      @graph.drawBold story.groups
    @points.classed \active (d, i) -> i is index

