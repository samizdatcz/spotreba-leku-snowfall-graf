data = ig.getData!
container = d3.select ig.containers.base
graphContainer = container.append \div
  ..attr \class \graph-container
graph = new ig.Graph graphContainer
storySelector = new ig.StorySelector container, graph, ig.stories, data
