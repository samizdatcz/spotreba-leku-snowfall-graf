tooltip = new Tooltip!
  ..watchElements!
formatNumber = (num) ->
  if num % 1
    ig.utils.formatNumber num, 1
  else
    ig.utils.formatNumber num, 0

vysvetlivky = {}
for {skupina, detail} in d3.tsv.parse ig.data.vysvetlivky
  vysvetlivky[skupina] = detail

years =
  | ig.isOld => [1970 to 2013]
  | _        => [2003 to 2013]

class ig.Graph extends ig.BaseGraph
  (@parentElement) ->
    super ...
    @margin.top = 20
    @margin.left = 100
    @margin.bottom = 40
    @margin.right = 20
    @displayedProperty = \prices
    @datapointAccessor = ~> it[@displayedProperty]
    @voronoi = new ig.VoronoiOverlay @
    @graphTip = new ig.GraphTip @
    @legendElement = @parentElement.append \div
      ..attr \class \legend
    @legendHeading = @legendElement.append \h3
    @legendHeadingContent = @legendHeading.append \span .attr \class "content"
    @legendHeading.append \span .attr \class \back .html "Zpět"
      ..on \click ~> if @lastDrawValue.parent then @draw that
    @legendList = @legendElement.append \ul
    @yearHighlight = @parentElement.append \div
      ..attr \class "year-highlight disabled"
    @xAccessor = (.x)
    @yAccessor = (.y)
    @drawDisplaySwitcher!

  prepareAxes: ->
    super ...
    @xAxis.tickFormat (yearIndex) -> years[Math.round yearIndex]
    @yAxis.ticks 8
    @yAxis.tickFormat (value) ~>
      humanValue =
        | value == 0 => ""
        | value < 1e3 => "#{formatNumber value}"
        | value < 1e8 => "#{formatNumber value / 1e6} mil."
        | value < 1e11 => "#{formatNumber value / 1e9} mld."
        | value < 1e14 => "#{formatNumber value / 1e12} bil."
        | value < 1e17 => "#{formatNumber value / 1e15} bld."
      if humanValue == ""
        ""
      else if @displayedProperty is \prices
        humanValue + " Kč"
      else if ig.isOld
        humanValue + " bal."
      else
        humanValue + " DDD"

  updateAxes: ->
    @xAxisG.call @xAxis
    @yAxisG.transition!
      ..duration 1200
      ..call @yAxis

  lineUpdate: (lines) ->
    super ...
      ..duration 1200
    @lines.classed \downlight no

  pointUpdate: ->
    super ...
      ..duration 1200

  lineEnter: (lines) ->
    super ...
      ..style \opacity 0
      ..transition!
        ..delay 400
        ..duration 800
        ..style \opacity 1

  lineExit: (lines) ->
    lines.transition!
      ..duration 800
      ..style \opacity 0
      ..remove!

  setDisplay: (property) ->
    @displayedProperty = property
    @switcherInput.each (d, i) ->
      @checked = if d.id == property then true else false

  redrawDisplay: (property) ->
    @displayedProperty = property
    @draw @lastDrawValue

  draw: ->
    @resetYearHighlight!
    @lastDrawValue = it
    super it.children
    @voronoi.draw @datapoints
      ..on \mouseover ({point}) ~>
        @highlightPoint point
        @highlightLine point.line
      ..on \mouseout ({point}) ~>
        @downlightPoint point
        @downlightLine point.line
      ..on \click ({point}) ~>
        return unless point.line.children.length
        @draw point.line
      ..classed \no-subs ({point}) ~> point.line.children.length is 0
    @drawLegend it.children

  drawLegend: (lines) ->
    elements = lines.map (it, index) ~>
      lastValue: @datapointAccessor it .[*-1]
      index: index
      line: it
      name: it.name
    elements.sort (a, b) -> b.lastValue - a.lastValue
    @legendHeadingContent
      ..html lines.0.parent.name || "Druhy léků"
      ..attr \title lines.0.parent.name
    @legendElement.classed \highlighted no
    @legendElement.classed \first-level lines.0.parent.name is void
    @legendList.selectAll "li" .remove!
    @legendItems = @legendList.selectAll "li" .data elements .enter!append \li
      ..html -> it.name
      ..classed \no-subs ~> it.line.children.length is 0
      ..append \div
        ..attr \class \line-stub
        ..style \background-color ~> @lineColors it.index
      ..on \mouseover ~>
        @highlightLine it.line
      ..on \mouseout ~>
        @downlightLine it.line
      ..on \click ~>
        return if it.line.children.length is 0
        tooltip.hide!
        @draw it.line
      ..attr \data-tooltip -> vysvetlivky[it.line.id]

  highlightYear: (fromYear, toYear) ->
    fromIndex = years.indexOf fromYear
    toIndex = years.indexOf toYear
    fromX = @xScale fromIndex - 0.25
    toX = @xScale toIndex + 0.25
    @yearHighlight
      ..style \left "#{fromX}px"
      ..style \width "#{toX - fromX}px"
      ..classed \disabled no

  resetYearHighlight: ->
    @yearHighlight
      ..classed \disabled yes


  highlightPoint: (point) ->
    @points
      .classed \active no
      .filter (d, index) -> index is point.lineIndex
      .classed \active yes

    suffix = if @displayedProperty is \counts
      if ig.isOld then "balení" else "definovaných denních dávek"
    else
      "Kč"
    text = "#{point.line.name}, rok #{years[point.x]}:<br>#{ig.utils.formatNumber point.y} #{suffix}"
    @graphTip.display point, text

  highlightLine: (line) ->
    @lines
      .classed \active no
      .filter -> it is line
      .classed \active yes

  downlightPoint: ->
    @points.classed \active no
    @graphTip.hide!

  downlightLine: ->
    @lines.classed \active no

  drawBold: (groups) ->
    @lines
      .filter -> it.id not in groups
      .classed \downlight yes
    @legendElement.classed \highlighted yes
    @legendItems.classed \highlight ~> it.line.id in groups

  drawDisplaySwitcher: ->
    switches =
      * id: \counts human: if ig.isOld then "Balení" else "Denní dávky"
      * id: \prices human: "Ceny"
    form = @parentElement.append \form .attr \class \switcher
    form.append \ul .attr \class \switcher
      .selectAll \li .data switches .enter!append \li
        ..append \input
          ..attr \type \radio
          ..attr \name \chc-switcher
          ..attr \id ~> "chc-#{it.id}"
          # ..attr \checked (d, i) ~> if i == 0 then "checked" else void
          ..on \change ~> @redrawDisplay it.id
        ..append \label
          ..attr \for ~> "chc-#{it.id}"
          ..html (.human)
    @switcherInput = form.selectAll \input

