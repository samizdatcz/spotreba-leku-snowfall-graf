class ig.BaseGraph
  (@parentElement) ->
    @margin =
      top: 0
      bottom: 0
      left: 0
      right: 0

    @datapointAccessor = (d) -> d
    @xAccessor = (d, i) -> i
    @yAccessor = (d, i) -> d
    @xScale = d3.scale.linear!
    @yScale = d3.scale.linear!
    @lineColors = d3.scale.ordinal!
      ..range <[#8c510a #bf812d #35978f #01665e #c51b7d #de77ae #7fbc41 #4d9221 #762a83 #9970ab #b2182b #d6604d #4393c3 #2166ac #878787 #4d4d4d]>

  prepare: ->
    @fullWidth = @parentElement.node!offsetWidth
    @fullHeight = @parentElement.node!offsetHeight
    @width = @fullWidth - @margin.left - @margin.right
    @height = @fullHeight - @margin.top - @margin.bottom
    @xScale.range [0, @width]
    @yScale.range [@height, 0]
    @svg = @parentElement.append \svg
      ..attr \width @fullWidth
      ..attr \height @fullHeight
    @drawing = @svg.append \g
      ..attr \transform "translate(#{@margin.left}, #{@margin.top})"
    @line = d3.svg.line!
      ..x ~> @xScale @xAccessor ...
      ..y ~> @yScale @yAccessor ...
    @symbol = d3.svg.symbol!
      ..type \circle
      ..size 50
    @prepareAxes!
    @prepared = yes

  prepareAxes: ->
    @xAxis = d3.svg.axis!
      ..scale @xScale
      ..tickSize 5, 1
    @xAxisG = @svg.append \g
      ..attr \class "axis x"
      ..attr \transform "translate(#{@margin.left}, #{@margin.top + @height})"

    @yAxis = d3.svg.axis!
      ..scale @yScale
      ..orient "left"
      ..tickSize 5, 1
    @yAxisG = @svg.append \g
      ..attr \class "axis y"
      ..attr \transform "translate(#{@margin.left}, #{@margin.top})"

  draw: (data) ->
    @prepare! if not @prepared
    @datapoints = @getPoints data
    @setScaleDomains @datapoints
    @lines = @drawing.selectAll \g.line .data data, (d, i) -> d.id || i
    @lines.enter!call @~lineEnter
    @lines.exit!call @~lineExit
    @lines.call @~lineUpdate
    @points = @lines.selectAll \.point .data @datapointAccessor, (d, i) -> d.id || i
    @points.enter!call @~pointEnter
    @points.exit!call @~pointExit
    @points.call @~pointUpdate
    @updateAxes!

  updateAxes: ->
    @xAxisG.call @xAxis
    @yAxisG.call @yAxis

  getPoints: (data) ->
    points = []
    for line in data
      linePoints = @datapointAccessor line
      for point, lineIndex in linePoints
        x = @xAccessor point, lineIndex
        y = @yAccessor point, lineIndex
        points.push {x, y, line, lineIndex}
    points

  setScaleDomains: (points) ->
    @xScale.domain d3.extent points.map (.x)
    @yScale.domain [0, d3.max points.map (.y)]

  lineEnter: (lines) ->
    lines.append \g
      ..attr \class \line
      ..append \path .attr \class \line

  lineExit: (lines) ->
      lines.remove!

  lineUpdate: (lines) ->
    lines.select \path .transition!
      ..attr \d ~> @line @datapointAccessor ...
      ..attr \stroke (d, i) ~> @lineColors i

  pointEnter: (points) ->
    points.append \g
      ..attr \class \point
      ..append \path
        ..attr \d @symbol

  pointExit: (points) ->
    points.remove!

  pointUpdate: (points) ->
    points
      ..select \path
        ..attr \fill (d, i, ii) ~> @lineColors ii
        ..attr \stroke (d, i, ii) ~> @lineColors ii
    points.transition!
      ..attr \transform ~>
          x = @xScale @xAccessor ...
          y = @yScale @yAccessor ...
          "translate(#x, #y)"

  drawXAxis: ->
