ig.getData = ->
  lines = ig.data.leky.split "\n"
    ..shift!
    ..pop!
  output = []
  assoc = {"": children: []}
  for line in lines
    [id, name, ...cols] = line.split "\t"
    countsCols =
      | ig.isOld => [0, 44]
      | _        => [121, 132]
    counts = cols.slice countsCols[0], countsCols[1]
      .map (d, i) ->
        y: parseFloat d
        x: i
      .filter -> !isNaN it.y
    pricesCols =
      | ig.isOld => [44, 88]
      | _        => [77, 88]
    prices = cols.slice pricesCols[0], pricesCols[1]
      .map (d, i) ->
        y: parseFloat d
        x: i
      .filter -> !isNaN it.y
    # counts = cols[88 to 131].map parseFloat
    len = id.length
    parentLen = if len == 3 or len == 7 then len - 2 else len - 1
    parentId = id.substr 0, parentLen
    parent = assoc[parentId]
    children = []
    assoc[id] = obj = {id, name, prices, counts, parent, children}
    assoc[parentId].children.push obj
  assoc
